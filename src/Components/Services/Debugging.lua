------  INITIALIZATION
----  variable localization
local component = component
local PBCore = PBCore
local PBInfo = PBCore.getInformation()
local newObject = PBCore.newObject
local newObjectClass = PBCore.newObjectClass
local lookupAny = PBCore.lookupAny
local Enums = PBCore:depend("EnumsManager").Enums
local Utilities = PBCore:depend("Utilities")
local Scheduler = PBCore:depend("ThreadManager").Scheduler
PBCore:depend("LuaSignal")
local Logging = PBCore:depend("Logging")
local record = Logging.record
local root = root
local string = string
local formatString = string.format
local subString = string.sub
------  API
--  @param  argument,  string or table  typesToCheck
--  @desc  checks to see if argument matches typesToCheck
function root.checkType(argument, typesToCheck)
    if (PBInfo.deploy )
    local __type_argument = type(argument)
    local __type_typesToCheck = type(typesToCheck)
    if (__type_typesToCheck == "string") then
        typesToCheck = {typesToCheck}
    elseif not (__type_typesToCheck == "table") then
        return error(
            formatString(
                Enums.Fatal.IncorrectType,
                "table or string"
            )  
        )
    end
    --  check types
    for i = 1, #typesToCheck do
        if (typesToCheck[i] == __type_argument) then
            return
        end
    end
    --  if we got here, prepare to error
    local string = ""
    for i = 1, #typesToCheck do
        string = string .." or "
    end
    string = subString(
        string,
        0, 
        #string - 4
    )
    string = formatString(
        Enums.Fatal.IncorrectType,
        string,
        __type_argument
    )
    return error(string)
end
--  @param  string Message,  vararg ...
--  @desc  Creates a debug message for debugging and fixing issues
function root.debugRecord(Message, ...)
    record(
        Enums.EventType.Debug,
        Message,
        ...
    )
ready()
